<div align="center">
    <strong>Pandoc template to export to <i>HTML</i></strong>
</div>


*Note*: The template doesn't support a table of contents. It will be added in the future. 

# Introduction


<div align="center">
    <img src="./example/preview.png" width="90%">
    <p>Preview</p>
</div>

This template is based on the [Pandoc default template](https://github.com/jgm/pandoc-templates/blob/master/default.html5) and supports printing (exporting from HTML to PDF/to a printer via the browser).

Examples can be found in the [example](./example) folder.

## Features

- [x] Support for printing
- [x] Support for LaTeX
- [x] Support for images
- [x] Support for code blocks
- [x] Support for tables 
- [x] Dates (*maybe to improve*) 
- [x] Table of contents (*maybe to improve*)
- [x] Checkboxes at each section
  - [x] Configurable (on/off)
- [ ] Summary at the end of the document

**TODO**: Increase printing visibility for reviewing.

# Installation

After downloading [course.html](course.html), you have to add the template in your `~/.pandoc/templates` folder (create it if it doesn't exist).

Or you can use the following command (Linux):

```bash
mkdir -p ~/.pandoc/templates # Create the folder if it doesn't exist
mv course.html ~/.pandoc/templates
```

# Usage

## Configuration

In every markdown file, you have to add the following metadata:

```yaml
---
title: "Title"
author: "Author"
date: "Date"
checked: true # Optional : if you want to add checkboxes at each section
lang: "en" # Optional : default is "en", french is "fr"
---
```

You have to add this at the beginning of the file :

```markdown
---
...
---

# Title 1

...
```

It will configure how the document will be rendered.

## Pandoc

```bash
pandoc [markdown file] -o [output file.html] --template=course.html
```

### Example

```bash
pandoc README.md -o README.html --template=course.html
```

### LaTeX

To include LaTeX, the command is the following:

```bash
pandoc README.md -o README.html --template=course.html --mathjax
```