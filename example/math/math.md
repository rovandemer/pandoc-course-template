---
title: Application en ingénierie et programmation numérique
author: Censored
date: Octobre 2022
checked: true
--- 

## Résolution de systèmes d'équations linéaires

Soient deux matrices $A,B$ telles que $A\in\mathcal{M}_n(\mathbb{R})$ et $B\in\mathcal{M}_{n,1}(\mathbb{R})$ avec $n\in \mathbb{N}^*$.

On cherche à déterminer, si elle existe, la matrice $X\in\mathcal{M}_{n,1}(\mathbb{R})$ telle que :

$$AX=B$$

Nous présenterons deux méthodes : 

- La méthode de Jacobi
- La méthode de Gauss-Seidel

### Principe général

La méthode de Jacobi et la méthode de Gauss-Seidel sont des méthodes itératives pour résoudre des systèmes linéaires. 

Elles consistent à trouver une approximation de la solution du système linéaire en itérant sur une suite de matrices $X_0,X_1,X_2,...$.

À chaque itération, on calcule l'erreur relative entre la solution exacte et la solution approchée. 

#### Décomposition de la matrice

Soit $A\in\mathcal{M}_n(\mathbb{R})$ inversible de la forme : 

<!--
Matrice généralisée
-->

$$A=\begin{pmatrix}
a_{11} & a_{12} & \cdots & a_{1n} \\
a_{21} & a_{22} & \cdots & a_{2n} \\
\vdots & \vdots & \ddots & \vdots \\
a_{n1} & a_{n2} & \cdots & a_{nn}
\end{pmatrix}$$

On peut écrire :

$$A = \begin{pmatrix}
a_{11} & 0 & \cdots & 0 \\
0 & a_{22} & \cdots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \cdots & a_{nn}
\end{pmatrix} - \begin{pmatrix}
0 & 0 & \cdots & 0 \\
-a_{21} & 0 & \cdots & 0 \\
\vdots & \vdots & \ddots & \vdots \\
-a_{n1} & -a_{n2} & \cdots & 0
\end{pmatrix} - \begin{pmatrix}
0 & -a_{12} & \cdots & -a_{1n} \\
0 & 0 & \cdots & -a_{2n} \\
\vdots & \vdots & \ddots & \vdots \\
0 & 0 & \cdots & 0
\end{pmatrix}
$$

On posera $A = D - E - F$, avec $D,E,F$ les matrices ci-dessus.

Le système $Ax = b$ est équivalent à $(D-E-F)x = b$.

$$
\begin{aligned}
    (D-E-F)x&=b\\
    Dx - (E+F)x &= b\\
    Dx &= (E+F)x + b\\
\end{aligned}
$$

D'après le théorème de convergence suites de matrices, on en déduit des méthodes itératives de résolution de systèmes différentes pour la méthode de Jacobi et de Gauss-Seidel.

#### Utilisation de l'erreur

Elle est donnée par : 

$$p^{(k)}=\max_{i=1,2,\dots,n}\left| \bar{x}_i - \tilde{x}_i^{(k)} \right|$$

où $\bar{x}_i$ est la $i$-ème valeur de la solution exacte et $\tilde{x}_i^{(k)}$ est la $i$-ème valeur de la solution approchée à l'itération $k$.

Dans notre cas, $\bar{x}_i$ vaut toujours 1, donc on ré-écrit :

$$p^{(k)}=\max_{i=1,2,\dots,n}\left| 1 - \tilde{x}_i^{(k)} \right|$$

Les deux méthodes ne convergent pas forcément. Donc dans les deux cas, on arrête l'algorithme lorsque l'erreur relative est inférieure à une valeur donnée où lorsque le nombre d'itérations est supérieur à un nombre donné.

Dans notre cas, on pose $\epsilon=0.01$ (la marge d'erreur) et $MAX=100$ (le nombre maximal d'itérations).

### Méthode de Jacobi

La méthode de Jacobi utilise la formule de récurrence suivante : 

$$Ax=b\Leftrightarrow Dx^{(k+1)}=(E+F)x^{(k)} + b$$

À l'étape $k$ de l'algorithme, on a $x^{(k)}$ comme approximation de la solution.

On obtient :

$$x_i^{(k+1)}=\frac{1}{a_{ii}}\left[b_i - \sum_{j\neq i}a_{ij}x_j^{(k)}\right], i=1,\dots,n$$

### Méthode de Gauss-Seidel

La méthode de Gauss-Seidel utilise la formule de récurrence suivante : 

$$Ax=b\Leftrightarrow Dx^{(k+1)}=E^{(k+1)} + Fx^{(k)} + b$$

À l'étape $k$ de l'algorithme, on a $x^{(k)}$ comme approximation de la solution.

On obtient :


$$x_i^{(k+1)}=\frac{1}{a_{ii}}\left[ b_i - \sum_{j_{(i > j)}}a_{ij}x_j^{(k+1)} -  \sum_{j_{(i < j)}}a_{ij}x_j^{(k)} \right], i=1,\dots,n$$


#### Optimisation de la formulation mathématiques de Gauss-Seidel 

On cherche à montrer qu'effectuer l'opération mathématique :

Pour chaque ligne $i$ de la matrice $x$ :
$$x_i^{(k+1)} = \frac{1}{a_{ii}}\left[b_i - \sum_{j_{(i>j)}} a_{ij}x_{j}^{(k+1)} - \sum_{j_{(i<j)}} a_{ij}x_{j}^{(k)}  \right]$$

s'écrivant en C :

Pour chaque ligne $i$ de la matrice $x$:
```C
sum = 0;

for (j=0;j<n;j++){

    if (i > j){

        sum += matrix[i][j] * unknownX[j]; 
        // unknownX est x à l'itération k+1 dans la formule mathématique


    } else if (i < j) {

        sum += matrix[i][j] * prevUnknown[j]; 
        // unknownX est x à l'itération k dans la formule mathématique

    }

}

unknownX[i] = ( vector[i] - sum ) / matrix[i][i];
```

est équivalente à, dans notre code C :

Pour chaque ligne $i$ de la matrice $x$:
```C

sum = 0;

for (j=0;j<n;j++){

    if (i != j){

        sum += matrix[i][j] * unknownX[j]; 
        // unknownX vaut x à l'itération k au début du programme

    }   

}

unknownX[i] = ( vector[i] - sum ) / matrix[i][i]; 
/*
Les valeurs du vecteurs valent x_i à l'itération k+1 
 -> pour toute ligne inférieure ou égale à i
 -> x_i à l'itération k pour toute ligne supérieur à i
*/
```


On montrera cela par récurrence sur les lignes de la matrice:

##### Initialisation

Avant la première itération, notre matrice $unknownX$ et $prevUnknownX$ sont les mêmes (le vecteur nul)

$i = 0$ donc $i < j$ (ou $j = 0$)

Donc on se retrouve toujours dans la seconde condition du programme naïf

Donc, pour $i = 0$, on a que $i < j$ et $i \neq j$ sont les mêmes conditions, donc on effectue les mêmes opérations à la première itération dans les deux programmes.

##### Récurrence

On suppose que nos deux programmes sont équivalents à une itération $i$.

Puisqu'on effectue des opérations ligne par ligne sur unkownX, on a que notre vecteur $unknownX$ vaut avant l'itération $i$:

$$
unknownX_i =
\begin{pmatrix}
x_o^{(k+1)} \\
x_1^{(k+1)} \\
⋮ \\
x_{i-1}^{(k+1)} \\
x_i^{(k)} \\
x_{i+1}^{(k)} \\
⋮ \\
x_n^{(k)} \\
\end{pmatrix}
$$

- La somme $\sum_{j_{i>j}} a_{ij}x_{j}^{(k+1)}$ s'intéresse uniquement au $x_j$ avec $j<i$, donc elle s'intéresse uniquement au valeur de $unknownX$ pour les lignes $j \leq i+1$,
or ces valeurs là sont celles de $x^{k+1}$ dans le vecteur $unknownX$

Donc les instructions pour $i > j$ sont équivalentes dans les 2 programmes, car on effectue les mêmes opérations.

- La somme $\sum_{j_{i<j}} a_{ij}x_{j}^{(k)}$ s'intéresse uniquement au $x_j^{(k)}$ avec $j>i$, donc elle s'intéresse uniquement au valeur de $unknownX$ pour les lignes $j \geq i+1$,
or ces valeurs là sont celles de $x^{(k)}$ dans le vecteur $unknownX$. Or les instructions dans le programme naïf sont de prendre les éléments dans $prevUnknownX$ car on cherche à récupérer uniquement des valeurs de l'étape $k$, mais dans $unknownX$, elles sont toujours à cette même étape.

Donc les instructions pour $i < j$ sont équivalentes dans les 2 programmes, car on effectue les mêmes opérations

On effectue donc les mêmes opérations dans les 2 programmes à l'itération $i$, par réunion des deux cas.

Donc, les deux programmes sont les mêmes.

### Présentation des programmes

Pour les deux méthodes, nous utiliserons les fichiers auxiliaires suivants :


- `memory-management.c` : allocations et libérations des matrices
- `matrix-operations.c` : définition du produit matriciel et de l'erreur relative

Puis nous aurons deux programmes principaux (`jacobi.c` et `gauss-seidel.c`) contenants les fonctions principales, ainsi que des fonctions communes suivantes :


- `fillMatrix` et `fillVector` : remplit une matrice à l'aide d'entrées utilisateur
- `showMatrix` et `showVector` : affichage des résultats du programme


#### Inputs / Output 

Entrées : 

- Taille de la matrice
- Marge d'erreur
- Matrice ligne par ligne
- Vecteur solution en ligne

Sorties : 

- L'avancée du programme
- Le nombre d'itérations
- L'erreur relative
- Le résultat

##### Exemple :

En entrée :

```
3
0.01
 4.000000  1.000000  1.000000 
 2.000000 -9.000000  0.000000 
 0.000000 -8.000000  6.000000 
6.0 -7.0 -2.0 
```

On obtient en sortie :

```
Quelle est la taille de ta matrice ?
n = 3

Quel est l'écart avec la solution exacte autorisé ?
Valeurs du vecteur b : 
Matrice : 
  4.000000  1.000000  1.000000	  6.000000
  2.000000 -9.000000  0.000000	 -7.000000
  0.000000 -8.000000  6.000000	 -2.000000


Iterations : 7

p(J) :   0.009297
Résultat : 

  1.006087

  0.999962

  0.990703

Affichage du produit Ax :
  4.000000  1.000000  1.000000	  6.015013
  2.000000 -9.000000  0.000000	 -6.987483
  0.000000 -8.000000  6.000000	 -2.055479
```


### Comparaison numérique des deux méthodes

| $A_i$ | $p(J)$ | $nbIterationsJ$ | $p(GS)$ | $nbIterationsGS$ |
|---|---|---|---|---|
|$A_1$| diverge | MAX | diverge | MAX |
|$A_2$| diverge | MAX | diverge | MAX |
|$A_3$| 0.009297 | 7 | 0.009297 | 3 |
|$A_4$| 0.005031 | 14 | 0.005031 | 20 |
|$A_5\in\mathcal{M}_3(\mathbb{R})$   | 0.007153 | 9 | 0.007155 | 5 |
|$A_5\in\mathcal{M}_6(\mathbb{R})$   | 0.005957 | 10 | 0.003967 | 6 |
|$A_5\in\mathcal{M}_8(\mathbb{R})$   | 0.006123 | 10 | 0.004083 | 6 |
|$A_5\in\mathcal{M}_{10}(\mathbb{R})$| 0.006161 | 10 | 0.004108 |6 |
|$A_6\in\mathcal{M}_3(\mathbb{R})$   | 0.007707 | 13 | 0.009634 | 6 |
|$A_6\in\mathcal{M}_6(\mathbb{R})$   | 0.008070 | 33 | 0.009007 | 15 |
|$A_6\in\mathcal{M}_8(\mathbb{R})$   | 0.009589 | 46 | 0.008593 | 21 |
|$A_6\in\mathcal{M}_{10}(\mathbb{R})$ | 0.009896 | 58 | 0.009151 | 26 |

Erreur tolérée : $0.01$.

#### Comparaison du nombre d'itérations des deux méthodes

##

![](Exercice%201.png)

![](Exercice%202%20-%20Matrice%201.png)

![](Exercice%202%20-%20Matrice%202.png)

#### Comparaisons des erreurs relatives des deux méthodes

##

![](./data/outputs/img/input1.png)

![](./data/outputs/img/input2.png)


![](./data/outputs/img/input3.png)

![](./data/outputs/img/input4.png)


![](./data/outputs/img/A5-3.png)

![](./data/outputs/img/A5-6.png)


![](./data/outputs/img/A5-8.png)

![](./data/outputs/img/A5-10.png)


![](./data/outputs/img/A6-3.png)

![](./data/outputs/img/A6-6.png)


![](./data/outputs/img/A6-8.png)

![](./data/outputs/img/A6-10.png)


#### Synthèse

Le jeu de test ci-dessus permet de constater que globalement, la méthode de Gauss-Seidel est plus efficace, surtout quand la matrice devient grande (on peut le voir sur la **figure 3**).

Il y a deux matrices qui ne convergeaient pas parmis la liste des inputs. On peut donc supposer que les méthodes ne convergeaient pas dans pas mal cas.

Comme les matrices $A_5$ et $A_6$ sont construites de la même façon, et que la convergence de la méthode semble être fortement liée à la diagonale de la matrice (cf Cours), on peut supposer que la propriété de l'existence de la convergence de la matrice est la même.