---
title: Table test
author: VAN DE MERGHEL Robin
date: 2023
lang: fr
---

| A | B | C |
|---|---|---|
| 1 | 2 | 3 |
| 4 | 5 | 6 |
| 7 | 8 | 9 |
| 10| 11| 12|


| Name | Surname | Id |
|------|---------|----|
| John | Doe     | $\sum_{n=0}^{10}n+1 = \pi$ |
| Mary | Smith   | $1 + 12 - \pi$  |
| Todd | Riddle  | 3  |