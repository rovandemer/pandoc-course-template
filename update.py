# 
# Script pour mettre à jour les markdown -> html
#
# Lit récursivement les fichiers markdown dans le dossier courant et ses sous-dossiers, et compile avec pandoc et la template "course.html" 
# Chaque markdown, compile et génère un fichier html dans le même dossier avec le même nom.
#
# ~ Commande de compilation :
# ~> pandoc [...].md -o [...].html --template=course.html --mathjax
# 
# Cf : https://gitlab.isima.fr/rovandemer/pandoc-course-template
import os
import sys
import subprocess

# Chemin vers le dossier courant
current_dir = os.path.dirname(os.path.realpath(__file__))

# Compile un fichier markdown en html avec pandoc
def compile_file(path: str):
    # Chemin vers le fichier html de sortie (remplace .md par .html)
    out_path = os.path.splitext(path)[0] + ".html"
    # Commande de compilation
    cmd = ["pandoc", path, "-o", out_path, "--template=course.html", "--mathjax"]
    # Compilation
    subprocess.run(cmd)
    # Log
    print(f"Compiled {path} -> {out_path}")

# Lit récursivement les fichiers markdown dans le dossier courant et ses sous-dossiers
def read_files(dir: str):
    for file in os.listdir(dir):
        path = os.path.join(dir, file)
        if os.path.isdir(path):
            read_files(path)
        elif file.endswith(".md"):
            # Sauf les README.md
            if file.lower() == "readme.md":
                continue 
            compile_file(path)

# Lit les fichiers markdown dans le dossier courant
read_files(current_dir)